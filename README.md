## 🔖&nbsp; Sobre

Desafio técnico de testes automatizados TexoIT

- No término da execução dos testes o relatório é gerado na pasta target/cucumber-reports

---

## 🚀 Tecnologias utilizadas

O projeto foi desenvolvido utilizando as seguintes tecnologias

- [Java](https://www.java.com/pt-BR/)
- [Selenium](https://www.selenium.dev/)
- [Cucumber](https://cucumber.io/)
- [JUnit](https://junit.org/junit5/)
- [IntelliJ](https://www.jetbrains.com/pt-br/idea/)
- [CucumberReport](https://cucumber.io/docs/cucumber/reporting/)
---
## 💻 Padrões de projeto
- PageObject
- PageFactory

---

Requisitos de software e execução

- Java 8 + JDK 14 deve estar instalado.
- Maven instalado e configurado no path da aplicação.


- Clonar o projeto para sua máquina git clone https://gitlab.com/silverioisaque/desafiotexoit-e2e.git
- Abrir na sua IDE preferencial
- mvn clean install
---

![img.jpg](Screenshot_1.jpg)


![img_1.jpg](Screenshot_2.jpg)
