package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GuidePage {

    @FindBy (linkText = "/albums/1/photos")
    private WebElement albumsPhotos;

    @FindBy(xpath = "//pre[contains(text(),'\"title\": ')]")
    private WebElement albums;

    @FindBy(linkText = "Guide")
    private WebElement menuGuide;

    public void clicarMenuGuide(){
        menuGuide.click();
    }

    public String obterTextoAlbums(){
        String text = albums.getText();

        return text;
    }

    public void clicarAlbumPhotos(){
        albumsPhotos.click();
    }
}
