package stepsDefinitions;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;

import static utils.Utils.*;

public class Hooks {

    @Before
    public void setUP() {
    }

    @After
    public void tearDown(Scenario scenario) throws InterruptedException {
        Thread.sleep(1000);
        screenshot(scenario);
        driver.quit();
    }

}
