package stepsDefinitions;

import Guide.*;
import io.cucumber.java.pt.*;
import org.json.simple.JSONArray;
import pageObjects.GuidePage;

import static utils.Utils.*;

public class ValidateGuideSteps {

    private JSONArray jsonArray;
    private final Guide guide;

    public ValidateGuideSteps(){
        guide = new Guide();
    }

    @Dado("que acesso a pagina principal")
    public void queAcessoAPagina() {
        acessarPagina();
    }

    @E("clico no menu Guide")
    public void clicoNoMenuGuide() {
        Na(GuidePage.class).clicarMenuGuide();
    }

    @Quando("abro o link album")
    public void abroOLinkAlbum() {
        Na(GuidePage.class).clicarAlbumPhotos();
    }

    @E("Salvo os dados em um ArrayJSON")
    public void salvoOsDadosEmUmArrayJSON() {
        String textoAlbums = Na(GuidePage.class).obterTextoAlbums();
        jsonArray = guide.criarArrayJson(textoAlbums);
    }

    @Entao("devo validar os dados id {long} albumId {long} title {string} url {string} e thumbnailUrl {string}")
    public void devoValidarOsDadosIdAlbumIdTitleUrlEThumbnailUrl(long id, long albumId, String title, String url, String thumbnailUrl) {
        guide.validarAlbum(jsonArray, new Photo(albumId, id, title, url, thumbnailUrl));
    }

}
