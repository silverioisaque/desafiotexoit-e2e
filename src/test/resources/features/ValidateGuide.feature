#language: pt
#enconding: UTF-8

Funcionalidade: Validar Album Fotos

  @validar
  Cenario: Validar Album Fotos
    Dado que acesso a pagina principal
    E clico no menu Guide
    Quando abro o link album
    E Salvo os dados em um ArrayJSON
    Entao devo validar os dados id 6 albumId 1 title "accusamus ea aliquid et amet sequi nemo" url "https://via.placeholder.com/600/56a8c2" e thumbnailUrl "https://via.placeholder.com/150/56a8c2"
