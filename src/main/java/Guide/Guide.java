package Guide;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import static org.junit.Assert.assertEquals;


public class Guide {

    public JSONArray criarArrayJson(String dados) {
        try {
            return (JSONArray) new JSONParser().parse(dados);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject retornarDadosDoAlbum(JSONArray jsonArray, long id) {
        JSONObject jasonObject;

        for (int i = 0; i < jsonArray.size(); i++) {
            jasonObject = (JSONObject) jsonArray.get(i);
            if (jasonObject.get("id").equals(id)) {
                return jasonObject;
            }
        }
        return null;
    }

    public void validarAlbum(JSONArray jsonArray, Photo photo) {
        JSONObject json = retornarDadosDoAlbum(jsonArray, photo.getId());

        assertEquals(photo.getAlbumId(), json.get("albumId"));
        assertEquals(photo.getId(), json.get("id"));
        assertEquals(photo.getTitle(), json.get("title"));
        assertEquals(photo.getUrl(), json.get("url"));
        assertEquals(photo.getThumbnailUrl(), json.get("thumbnailUrl"));
    }
}
