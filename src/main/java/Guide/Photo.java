package Guide;

import lombok.Data;

@Data
public class Photo {
    private long albumId;
    private long id ;
    private String title;
    private String url;
    private String thumbnailUrl;

    public Photo(long albumId, long id, String title, String url, String thumbnailUrl) {
        super();
        this.albumId = albumId;
        this.id = id;
        this.title = title;
        this.url = url;
        this.thumbnailUrl = thumbnailUrl;
    }

}
